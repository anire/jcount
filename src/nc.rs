/// Module holding number crunching traits.
///
/// For now, there are two main types of number crunchers:
///     NumberCruncher<Read> - Consumes T and returns results.
///     StreamingNumberCruncher<Read> - {
///         Consumes all available input from T
///         and returns a partial result.
///     }
use filter::Filter;

// XXX Create StreamingNumberCruncher<T: Read>

/// Synchronous number cruncher.
pub trait NumberCruncher {
    type Result;

    fn crunch(&mut self, filter: Filter) -> Self::Result;
}
