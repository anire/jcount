/// Handles rendering a HashMap<(String, String), u32>
/// Should be able to do stuff like:
/// `rust
/// let renderer = Renderer::new(map);
/// loop {
///     wait_for_next_frame();
///     renderer.draw();
/// }
/// `
/// etc.
use std::{cmp::max, collections::HashMap};

const KEY_VALUE_HEADER: &'static str = "Key-value pair";
const OCCURRENCES_HEADER: &'static str = "# of occurrences";

pub struct Renderer;

/// Renderer.
/// Exposes a static method.
impl Renderer {
    pub fn draw<F>(map: &HashMap<(String, String), u32>, filter: F, should_sort: bool)
        where F: Fn((&String, &String), u32) -> bool {
        // Setup table.
        // Length of 'key-value' header.
        // Used as a fallback in case there are no keys.
        let kv_header_width = KEY_VALUE_HEADER.len();
        // Length of 'occurrences' header.
        // Used as a fallback in case there are no keys.
        let occurrences_header_width = OCCURRENCES_HEADER.len();
        // Figure out what the longest (k, v) pair's length is.
        let largest_kv_width = max(map.keys().map(|(ref k, ref v)| format!("({}, {})", k, v).len()).max().unwrap_or(kv_header_width),
            kv_header_width);
        // Figure out width of largest occurrence. (sounds weird)
        let largest_occurrences_width = max(map.values().max().map(|o| o.to_string().len()).unwrap_or(occurrences_header_width),
            occurrences_header_width);

        // 1. Print column headers.
        let fmt_column_headers = format!("{:w1$} | {:w2$}", KEY_VALUE_HEADER, OCCURRENCES_HEADER,
                                         w1 = largest_kv_width, w2 = largest_occurrences_width);
        println!("{}", fmt_column_headers);
        // 2. Print separator. (i.e. largest widths + 3)
        let fmt_separator = "-".repeat(largest_kv_width + largest_occurrences_width + 3);
        println!("{}", fmt_separator);
        // 3. Process rows.
        // Sort if asked to.
        let map = if should_sort {
            let v_map: Vec<(&(String, String), &u32)> = map.iter().collect();
            let mut s_map = v_map.into_boxed_slice();
            s_map.sort_by(|&(_, &c1), &(_, &c2)| {
                c2.cmp(&c1)
            });
            s_map
        } else {
            map.iter().collect::<Vec<(&(String, String), &u32)>>().into_boxed_slice()
        };
        for ((ref k, ref v), ref c) in map.iter() {
            // Create k, v string.
            let kv_str = format!("({}, {})", k, v);
            let fmt_string = format!("{:w1$} | {:w2$}", kv_str, c, w1 = largest_kv_width, w2 = largest_occurrences_width);
            if filter((&k, &v), **c) {
                println!("{}", fmt_string);
            }
        }
    }
}
