/// JSON number crunching logic.
/// ** NOTE ** This is NOT streaming.
/// (i.e. it will not work with an infinite stream of input)
///
/// Also note that it theoretically could never be possible
/// due to the nature of JSON and the fact that you can split
/// one object over multiple lines.

extern crate serde_json;
use serde_json::{from_reader, Value};

use filter::Filter;
use nc::NumberCruncher;
use std::io::Read;
use std::collections::HashMap;

/// JSON NumberCruncher.
/// Handles creating analytics.
/// TODO Store collected data in an Analytics object.
/// XXX Create NumberCruncher trait.
/// XXX Create Filter object.
pub struct JSONNumberCruncher {
    json_value: Value,
}

impl NumberCruncher for JSONNumberCruncher {
    type Result = Result<HashMap<(String, String), u32>, String>;

    fn crunch(&mut self, filter: Filter) -> Self::Result {
        let mut keys = Vec::new();
        let mut include_keys = Vec::new();
        let mut occurrences = HashMap::new();
        let arr = if let Some(a) = self.json_value.as_array() {
            a
        } else {
            return Err("Failed to convert JSON value into array!".to_owned());
        };
        for k in arr.iter().cloned().nth(0).expect("No objects in array").as_object().expect("Objects not in array!").keys() {
            keys.push(k.clone());
        }
        if let Some(ikeys) = filter.keys() {
            for k in ikeys {
                let a = k.to_owned();
                if a == "*" {
                    include_keys.extend(keys);
                    break;
                }
                if keys.contains(&a) {
                    include_keys.push(a);
                }
            }
        } else {
            include_keys.extend(keys);
        }
        for obj in arr.iter() {
            match obj.as_object() {
                None => return Err("JSON Array not formatted properly.".to_owned()),
                Some(obj) => {
                    for (k, v) in obj.iter() {
                        let mut got_str_value = false;
                        let mut str_value = String::new();
                        match v {
                            Value::Bool(ref vv) => {
                                got_str_value = true;
                                str_value = vv.to_string();
                            },
                            Value::Number(ref vv) => {
                                got_str_value = true;
                                str_value = vv.as_f64().unwrap().to_string();
                            },
                            Value::String(ref s) => {
                                str_value = s.to_owned();
                                got_str_value = true;
                            }, _ => {}
                        }
                        if got_str_value {
                            occurrences.entry((k.clone(), str_value))
                                .and_modify(|v| { *v += 1 })
                                .or_insert(1);
                        }
                    }
                }
            }
        }
        Ok(occurrences)
    }
}

impl JSONNumberCruncher {
    pub fn new<T: Read>(reader: T) -> Self {
        // Attempt to parse T.
        let val: Value = match serde_json::from_reader(reader) {
            Ok(v) => v,
            Err(e) => {
                println!("ERR: {}", e);
                ::std::process::exit(1)
            }
        };
        Self {
            json_value: val
        }
    }

    pub fn from_value(val: Value) -> Self {
        Self {
            json_value: val
        }
    }
}
