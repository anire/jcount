/// JSON lines number cruncher.
/// This IS streaming, as it is necessary
/// It provides intermediate results
/// by reading a certain number of lines (e.g. 1000)
///
/// This actually just constructs a Value::Array
/// from the object lines and passes it to JSONNumberCruncher.
/// Nothing too hard.

extern crate serde_json;
use serde_json::Value;

use filter::Filter;
use nc::NumberCruncher;
use json::JSONNumberCruncher;
use std::io::BufRead;
use std::collections::HashMap;

/// JSON lines NumberCruncher.
/// Additional method which reads in more lines.
/// And adds them to big array object.
pub struct JSONLinesNumberCruncher<'a, T: 'a + BufRead> {
    read_buf: &'a mut T,
    arr_obj: Vec<Value>,
    cur_arr: Option<Value>,
}

impl<'a, T: 'a + BufRead> NumberCruncher for JSONLinesNumberCruncher<'a, T> {
    type Result = Result<HashMap<(String, String), u32>, String>;

    fn crunch(&mut self, filter: Filter) -> Self::Result {
        // Update cur_arr
        self.cur_arr = Some(Value::Array(self.arr_obj.clone()));
        let mut cruncher = JSONNumberCruncher::from_value(self.cur_arr.clone().unwrap());
        cruncher.crunch(filter)
    }
}

impl<'a, T: 'a + BufRead> JSONLinesNumberCruncher<'a, T> {
    pub fn new(rdr: &'a mut T) -> Self {
        Self {
            read_buf: rdr,
            arr_obj: Vec::new(),
            cur_arr: None,
        }
    }

    pub fn read_lines(&mut self, lines: u32) {
        for _ in 0..lines {
            let mut line = String::new();
            self.read_buf.read_line(&mut line);
            let val: Value = serde_json::from_str(&line).unwrap();
            self.arr_obj.push(val);
        }
    }
}
