extern crate clap;
extern crate serde_json;
use clap::{Arg, App};
use serde_json::{Value, from_reader};

use std::{
    fs::File,
    collections::HashMap,
    io::{
        stdin,
        stdout,
        BufReader,
        Write,
        Read,
    },
    process::exit,
}; 
mod render;
use render::Renderer;

mod json;
mod jsonlines;
mod filter;
mod nc;

fn main() {
    let matches = App::new("JCount")
        .version("0.1")
        .author("anire <anirudhqazxswced@gmail.com>")
        .about("Counts duplicates of key-value pairs in a JSON file.")
        .arg(Arg::with_name("FILE")
             .help("Sets the input file. If unset, reads from stdin.")
             .index(1))
        .arg(Arg::with_name("keys")
             .help("Determines which keys to show. Ignores keys not in the file.")
             .short("k")
             .long("keys")
             .multiple(true)
             .takes_value(true))
        .arg(Arg::with_name("ones")
             .help("Show key-value pairs with only one occurrence.")
             .short("1")
             .multiple(true)
             .long("show-ones"))
        .arg(Arg::with_name("unsorted")
             .help("Do not sort table by number of occurrences.")
             .short("u")
             .long("unsorted"))
        .arg(Arg::with_name("jsonlines")
             .help("Interpret input as JSON Lines.")
             .short("j")
             .long("json-lines"))
        .arg(Arg::with_name("linebuffer")
             .help("Number of lines to read with each pass. Ignored without --json-lines.")
             .short("l")
             .long("line-buffer")
             .default_value("10"))
        .get_matches();

    let stdin = stdin();
    let mut file_val = BufReader::new(Box::new(stdin.lock()) as Box<Read>);
    if matches.is_present("FILE") {
        let f = File::open(matches.value_of("FILE").unwrap()).unwrap_or_else(|e| {
            println!("Could not open file. Make sure it exists.");
            println!("Error from File::open: {}", e);
            exit(1);
        });
        file_val = BufReader::new(Box::new(f) as Box<Read>);
    }
    use nc::NumberCruncher;
    let filter = if matches.is_present("keys") {
        let keys = matches.values_of("keys").unwrap().clone();
        filter::Filter::Keyed(keys.map(|x| x.to_string()).collect::<Vec<String>>())
    } else {
        filter::Filter::None
    };
    let ones = matches.is_present("ones");

    if !matches.is_present("jsonlines") {
        let mut jnc = json::JSONNumberCruncher::new(file_val);

        let occurrences = jnc.crunch(filter.clone()).unwrap_or(HashMap::new());
        Renderer::draw(&occurrences, |(ref k, ref _v), o| {
            (ones || o > 1) && filter.matches(k.to_string())
        }, !matches.is_present("unsorted"));
    } else {
        let mut jlnc = jsonlines::JSONLinesNumberCruncher::new(&mut file_val);
        // Read first 100 lines
        jlnc.read_lines(100);

        // Get # of lines to read/pass
        let lines_per_pass = matches.value_of("linebuffer").unwrap().parse::<u32>().unwrap();

        loop {
            let occurrences = jlnc.crunch(filter.clone()).unwrap_or(HashMap::new());
            Renderer::draw(&occurrences, |(ref k, ref _v), o| {
                (ones || o > 1) && filter.matches(k.to_string())
            }, !matches.is_present("unsorted"));
            let stdout = stdout();
            stdout.lock().flush();
            jlnc.read_lines(lines_per_pass);
        }
    }
    
//    match file_val.as_array() {
//        None => println!("Your JSON is not formatted properly. JCount expects a list of objects."),
//        Some(a) => {
//            // 1. Key roundup.
//            for m in a.iter() {
//                match m.as_object() {
//                    None => {
//                        println!("Your JSON is not formatted properly. JCount expects a list of objects.");
//                        exit(1);
//                    },
//                    Some(o) => {
//                        for k in o.keys() {
//                            keys.push(k.clone());
//                        }
//                    }
//                }
//            }
//            // 2. keys argument processing.
//            for arg in matches.values_of("keys").unwrap() {
//                let a = arg.to_owned();
//                if a == "*" {
//                    include_keys.extend(keys);
//                    break;
//                }
//                if keys.contains(&a) {
//                    include_keys.push(a);
//                }
//            }
//            // 3. Number crunching.
//            for m in a.iter() {
//                match m.as_object() {
//                    None => {
//                        println!("Your JSON is not formatted properly. JCount expects a list of objects.");
//                        exit(1);
//                    },
//                    Some(o) => {
//                        for (k, v) in o.iter() {
//                            let mut got_str_value = false;
//                            let mut str_value = String::new();
//                            if let Some(n) = v.as_f64() {
//                                str_value = n.to_string();
//                                got_str_value = true;
//                            }
//                            if let Some(b) = v.as_bool() {
//                                str_value = b.to_string();
//                                got_str_value = true;
//                            }
//                            if let Some(s) = v.as_str() {
//                                str_value = s.to_owned();
//                                got_str_value = true;
//                            }
//                            if got_str_value {
//                                kv_occurrences.entry((k.clone(), str_value))
//                                    .and_modify(|v| { *v += 1 })
//                                    .or_insert(1);
//                            }
//                        }
//                    }
//                }
//            }
//            // 4. Print results.
//            // For now, no filtering.
//            Renderer::draw(&kv_occurrences, |(ref k, ref v), c| {
//                (ones || c > 1) && include_keys.contains(k)
//            }, !matches.is_present("unsorted"));
////             for ((ref k, ref v), c) in kv_occurrences.iter() {
////                 if (ones || c > &1) && include_keys.contains(k) {
////                     println!("Got key-value pair ({}, {}) {} times.", k, v, c);
////                 }
////             }
//        }
//    }
}
