/// Holds the Filter struct.
/// The Filter is responsible for specifying
/// what criteria we would like to search for.

type Key = String;

/// The Filter.
#[derive(PartialEq, Clone)]
pub enum Filter {
    None,
    /// Used to represent multiple criteria.
    Compound(Vec<Filter>),
    Keyed(Vec<Key>),
}

impl Filter {
    pub fn keys(self) -> Option<Vec<Key>> {
        match self {
            Filter::Compound(filters) => {
                for f in filters.iter().cloned() {
                    match f.keys() {
                        None => {},
                        Some(k) => return Some(k),
                    }
                }
                None
            }
            _ => None,
            Filter::Keyed(v) => Some(v),
        }
    }

    pub fn matches(&self, k: Key) -> bool {
        match self {
            Filter::None => true,
            _ => {
                let keys = self.clone().keys().unwrap();
                keys.iter().position(|v| v == &k).is_some()
            }
        }
    }
}
