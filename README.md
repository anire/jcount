# JCount
**Note: Not to be confused with jCount**
A Rust-based JSON analyzer. (WIP)
Currently, it supports analyzing a list of objects one level deep.
See ROADMAP.md for future features.

# Example
Try running `cat json_test2.json | cargo run` in your terminal.
```
$ cat json_test2.json | cargo run
[snipped cargo output for brevity]
Key-value pair     | # of occurrences
-------------------------------------
(status_code, 200) |              850
(url, /posts)      |              274
(url, /submit)     |              258
(url, /)           |              235
(url, /login)      |              233
(status_code, 500) |              150
(response_ms, 2)   |               85
(response_ms, 4)   |               81
(response_ms, 3)   |               63
(response_ms, 1)   |               61
(response_ms, 5)   |               59
(response_ms, 14)  |               33
(response_ms, 16)  |               33
(response_ms, 23)  |               30
(response_ms, 29)  |               29
(response_ms, 24)  |               29
(response_ms, 7)   |               28
(response_ms, 12)  |               28
(response_ms, 27)  |               27
(response_ms, 15)  |               27
(response_ms, 9)   |               27
(response_ms, 19)  |               26
(response_ms, 18)  |               25
(response_ms, 28)  |               24
(response_ms, 13)  |               24
(response_ms, 22)  |               24
(response_ms, 8)   |               24
(response_ms, 26)  |               23
(response_ms, 11)  |               23
(response_ms, 20)  |               22
(response_ms, 25)  |               21
(response_ms, 30)  |               19
(response_ms, 17)  |               17
(response_ms, 6)   |               17
(response_ms, 21)  |               15
(response_ms, 10)  |               15
(response_ms, 31)  |               11
(response_ms, 35)  |                9
(response_ms, 34)  |                8
(response_ms, 32)  |                7
(response_ms, 33)  |                6
```
Pretty cool, huh?

Nothing else in here for now
